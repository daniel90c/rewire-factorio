# rewire mod for factorio 0.18

![](rewire-tool_0.0.3/thumbnail.png)

A simple mod for Factorio that allows straightening cables between the power poles.
This is an update of a 0.16 mod from https://github.com/martinruefenacht/factorio_rewire

Its my first atempt at modding and scripting, so i dont think that i can solve all the problems, but its a mod that i was missing and it provided an easy task to fix it.