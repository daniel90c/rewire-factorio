
data:extend({
    {
      type = "selection-tool",
      name = "rewire-tool",
      order = 'c[selection-tool]-a[wire-cutter]',
      selection_color = {r = 1, g = 0, b = 0},
      alt_selection_color = {r = 0, g = 1, b = 0},
      selection_mode = {'same-force', 'buildable-type', 'items-to-place'},
      alt_selection_mode = {'same-force', 'buildable-type', 'items-to-place'},
      selection_cursor_box_type = 'copy',
      alt_selection_cursor_box_type = 'copy',
      icon="__rewire-tool__/graphics/tool.png",
      icon_size = 32,
      stack_size = 1,
      stackable = false,

      show_in_library = false,
      flags = {'hidden', 'only-in-cursor'},
      subgroup = 'tool',
    },
    --- shortcut added
    {
      name = "give-rewire-tool",
      type = "shortcut",
      order = "b[blueprints]-s[rewire-tool]",
      action = "create-blueprint-item",
      localised_name = {"item-name.rewire-tool"},
      item_to_create = "rewire-tool",
      style = "green",
      icon = {
        filename = "__rewire-tool__/graphics/rewire-tool-x32-white.png",
        flags = {
          "icon"
        },
        priority = "extra-high-no-scale",
        scale = 1,
        size = 32
      },
      small_icon = {
        filename = "__rewire-tool__/graphics/rewire-tool-x24.png",
        flags = {
          "icon"
        },
        priority = "extra-high-no-scale",
        scale = 1,
        size = 24
      },
      disabled_small_icon = {
        filename = "__rewire-tool__/graphics/rewire-tool-x24-white.png",
        flags = {
          "icon"
        },
        priority = "extra-high-no-scale",
        scale = 1,
        size = 24
      },
      associated_control_input = "give-rewire-tool"
    },
    {
      name = "give-rewire-tool",
      type = "custom-input",
      key_sequence = "ALT + R",
      action = "create-blueprint-item",
      item_to_create = "rewire-tool",
      consuming = "game-only"
    }
  })