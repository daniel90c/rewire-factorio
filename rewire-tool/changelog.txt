---------------------------------------------------------------------------------------------------
Version: 0.0.6
Date: 2020-08-09
  Changes:
    - fix strings on english.
  Locale:
    - Add Spanish Translations
---------------------------------------------------------------------------------------------------
Version: 0.0.5
Date: 2020-03-17
  Bugfixes:
    - updated to factorio version 0.18.13
    - added shortcut and icon graphics
---------------------------------------------------------------------------------------------------
Version: 0.0.4
Date: 2019-08-15
  Changes:
    - updated to factorio version 0.18
    - tweaked graphics from source
---------------------------------------------------------------------------------------------------
2017Version: 0.0.3
Date: 2019-08-15
  Major Features:
    - Support for factorio version 0.17, initial release based on the mod by deltatag https://mods.factorio.com/mod/rewire